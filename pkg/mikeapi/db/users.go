package db

import (
	"database/sql"
	"mikeapi/pkg/mikeapi/models"
)

func (db Database) GetAllUsers() (*models.UserList, error) {
	list := &models.UserList{}
	rows, err := db.Conn.Query("SELECT * FROM users")
	if err != nil {
		return list, err
	}
	for rows.Next() {
		var user models.User
		err := rows.Scan(&user.DiscordId, &user.LfmUsername, &user.Birthday)
		if err != nil {
			return list, err
		}
		list.Users = append(list.Users, user)
	}
	return list, nil
}

func (db Database) AddUser(user *models.User) error {
	query := `INSERT INTO users (discord_id, lfm_username, birthday) VALUES ($1, $2, $3)`
	err := db.Conn.QueryRow(query, user.DiscordId, user.LfmUsername, user.Birthday).Scan()
	if err != nil {
		return err
	}
	return nil
}

func (db Database) GetUserById(DiscordId string) (models.User, error) {
	user := models.User{}
	query := `SELECT * FROM users WHERE id = $1`
	row := db.Conn.QueryRow(query, DiscordId)
	switch err := row.Scan(&user.DiscordId, &user.LfmUsername, &user.Birthday); err {
	case sql.ErrNoRows:
		return user, ErrNoMatch
	default:
		return user, err
	}
}

func (db Database) DeleteUser(discordId int) error {
	query := `DELETE FROM users WHERE id = $1;`
	_, err := db.Conn.Exec(query, discordId)
	switch err {
	case sql.ErrNoRows:
		return ErrNoMatch
	default:
		return err
	}
}

func (db Database) UpdateUser(discordId int, userData models.User) (models.User, error) {
	user := models.User{}
	query := `UPDATE items SET discord_id=$1, lfm_username=$2 birthday=$3 WHERE discord_id=$4 RETURNING discord_id, lfm_username, birthday;`
	err := db.Conn.QueryRow(query, userData.DiscordId, userData.LfmUsername, userData.Birthday, discordId).Scan(&user.DiscordId, &user.LfmUsername, &user.Birthday)
	if err != nil {
		if err == sql.ErrNoRows {
			return user, ErrNoMatch
		}
		return user, err
	}
	return user, nil
}
