CREATE TABLE IF NOT EXISTS users (
    discord_id TEXT PRIMARY KEY,
    lfm_username TEXT,
    birthday TEXT
);
CREATE TABLE IF NOT EXISTS servers (
    discord_id TEXT PRIMARY KEY,
    mute_role TEXT,
    logs_channel TEXT,
    welcome_channel TEXT,
    logs BOOLEAN DEFAULT FALSE,
    welcome BOOLEAN DEFAULT FALSE
);
CREATE TABLE IF NOT EXISTS members (
    member_id SERIAL PRIMARY KEY,
    warns SMALLINT DEFAULT 0,
    server_id TEXT NOT NULL REFERENCES servers(discord_id),
    user_id TEXT NOT NULL REFERENCES users(discord_id)
);