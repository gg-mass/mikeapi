package db

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

var ErrNoMatch = fmt.Errorf("not matching user")

type Database struct {
	Conn *sql.DB
}

func Initialize(username, password, name, host string, port int) (Database, error) {
	db := Database{}
	dsn := fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		host, port, username, password, name,
	)
	conn, err := sql.Open("postgres", dsn)
	if err != nil {
		return db, err
	}
	db.Conn = conn
	err = db.Conn.Ping()
	if err != nil {
		return db, err
	}
	return db, nil
}
