package models

import (
	"fmt"
	"net/http"
)

type Member struct {
	ID       uint   `json:"id"`
	Warns    uint   `json:"warns"`
	ServerId string `json:"server_id"`
	UserId   string `json:"user_id"`
}

type MemberList struct {
	Members []Member `json:"members"`
}

func (m *Member) Bind(r *http.Request) error {
	if m.ServerId == "" || m.UserId == "" {
		return fmt.Errorf("server/user is a required field")
	}
	return nil
}

func (*MemberList) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

func (*Member) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}
