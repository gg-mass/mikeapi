package models

import (
	"fmt"
	"net/http"
)

type User struct {
	DiscordId   string `json:"discord_id"`
	LfmUsername string `json:"lfm_username"`
	Birthday    string `json:"birthday"`
}

type UserList struct {
	Users []User `json:"users"`
}

func (u *User) Bind(r *http.Request) error {
	if u.DiscordId == "" {
		return fmt.Errorf("discord_id is a required field")
	}
	return nil
}

func (*UserList) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

func (*User) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}
