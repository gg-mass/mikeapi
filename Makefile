GOLANG_PKG := mikeapi

clean:
	sudo docker rm -f ${GOLANG_PKG} || true

docker-up:
	docker-compose up -d --build

docker-down:
	docker-compose down

build:
	go build -o ./bin/${GOLANG_PKG} ./cmd/${GOLANG_PKG}/main.go

run:
	go run ./cmd/${GOLANG_PKG}/main.go
