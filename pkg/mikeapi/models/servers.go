package models

import (
	"fmt"
	"net/http"
)

type Server struct {
	DiscordId      string `json:"discord_id"`
	MuteRole       string `json:"mute_role"`
	LogsChannel    string `json:"logs_channel"`
	WelcomeChannel string `json:"welcome_channel"`
	Logs           bool   `json:"logs"`
	Welcome        bool   `json:"welcome"`
}

type ServerList struct {
	Servers []Server `json:"servers"`
}

func (s *Server) Bind(r *http.Request) error {
	if s.DiscordId == "" {
		return fmt.Errorf("discord_id is a required field")
	}
	return nil
}

func (*ServerList) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

func (*Server) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}
